/*
 * @Author: your name
 * @Date: 2021-12-06 13:21:40
 * @LastEditTime: 2022-12-27 13:16:12
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\index.d.ts
 */

declare module "*.svg"
declare module "*.png"
declare module "*.jpg"
declare module "*.jpeg"
declare module "*.gif"
declare module "*.bmp"
declare module "*.tiff"
declare module "watermark-component-for-react"