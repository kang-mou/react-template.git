/*
 * @Author: your name
 * @Date: 2021-11-24 14:35:24
 * @LastEditTime: 2023-05-07 22:30:20
 * @LastEditors: 康某 carl0608@126.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\home\index.tsx
 */
import { ReactElement, useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import HeaderCom from "./compontents/Header";
import Sider from "./compontents/Sider";
import "./index.less";
const InlineLay = (): ReactElement => {
	const navigate = useNavigate();
	useEffect(() => {
		console.log(location);

		if (location.pathname === "/") {
			navigate("/dashboard");
		}
	}, []);
	return <div className="inline">
		<Sider></Sider>
		<div className="app-content">
			<div className="app_header">
				<HeaderCom></HeaderCom>
			</div>
			<div className="app-content-box">
				<Outlet></Outlet>
			</div>
		</div>
	</div>;
};
export default InlineLay;