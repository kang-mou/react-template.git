/*
 * @Author: your name
 * @Date: 2021-12-02 18:08:06
 * @LastEditTime: 2023-05-08 16:58:20
 * @LastEditors: kang_mou kangh@gov-security.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\components\Header\index.tsx
 */
import { BellOutlined, ClearOutlined, DownOutlined, ExceptionOutlined, FileDoneOutlined, MenuFoldOutlined, MenuUnfoldOutlined, QuestionCircleOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Badge, Breadcrumb, Button, Drawer, Dropdown, Space, Tabs, notification } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";

// import "../../locales/i18n";
import { RouteItemsType, siderRoutes } from "../../../router";
import { RootContext } from "../../../store";
import { _findRouteByLocation } from "../../../utils/index";
import "./index.less";
const HeaderCom = (): React.ReactElement => {
	const { t, i18n } = useTranslation();
	const { state, dispatch } = useContext(RootContext);
	const { siderCollapsed } = state;
	const [list, setList] = useState<RouteItemsType[]>([]);
	const location = useLocation();
	const [badgeNum, setBadgeNum] = useState<number>(9);
	const [visibile, setVisibile] = useState<boolean>(false);
	const [currentLang, setCurrentLang] = useState("");
	const DropdownItems: MenuProps["items"] = [
		{
			label: t("zh"),
			key: "zh"
		},
		{
			label: t("en"),
			key: "en"
		}
	];
	const messageOption = [
		{
			label: t("header.inform"),
			value: 4,
			key: "inform",
			Icon: ExceptionOutlined
		},
		{
			label: t("header.message"),
			value: 3,
			key: "message",
			Icon: FileDoneOutlined
		},
		{
			label: t("header.todo"),
			value: 0,
			key: "todo",
			Icon: ClearOutlined
		}
	];

	const renderMessage = () => {

	};

	/* 递归获取 面包屑 */
	const getBreadByLocation = (): void => {
		let defaultArr = [];
		let faRouter = [];
		defaultArr = siderRoutes.filter(item => {
			if (item.path === location.pathname) {
				return true;
			} else {
				const obj: RouteItemsType = item;
				if (item.children) {
					faRouter = item.children.filter(item2 => {
						if (item2.path === location.pathname) {
							faRouter.push(obj);
							return true;
						} else {
							return false;
						}
					});
					if (faRouter && faRouter.length) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		});
		setList([...faRouter, ...defaultArr]);
	};


	// 切换语言
	const handleDropdown = (e) => {
		console.log(e);
		const { key } = e;
		i18n.changeLanguage(key);
		dispatch({ type: "SET_LANGUAGE", data: key });
	};

	// 跳转antd 4.x 官网
	const handleTo4xDesign = () => {
		window.open("https://4x.ant.design/components/icon-cn/");
	};

	// 关闭消息弹框
	const drawerClose = () => {
		setVisibile(false);
	};
	// 打开消息弹框
	const showMessage = () => {
		setVisibile(true);
	};

	// 监听页面 失焦、聚焦
	const handleVisibilityChange = (e) => {
		const { target } = e;
		if (target.visibilityState === "hidden") {
			let num = badgeNum + 1;
			setBadgeNum(num);
			console.log(badgeNum, "页面聚焦");
		} else if (target.visibilityState === "visible") {
			console.log(badgeNum, "页面失焦");
		}
	};

	const openNotification = () => {
		const key = `open${Date.now()}`;
		console.log(state.language, t, "====");
		const btn = (
			<Button type="primary" size="small" onClick={() => notification.destroy(key)}>
				{t("close")}
			</Button>
		);
		notification.open({
			message: t("header.thank"),
			description: t("header.vue"),
			btn,
			placement: "top",
			key,
			onClose: close,
		});
	};

	/* 监听路由变化 */
	useEffect(() => {
		setList(_findRouteByLocation(location.pathname));
	}, [location.pathname]);

	useEffect(() => {
		// 需要监听 badgeNum 重新挂载
		window.addEventListener("visibilitychange", handleVisibilityChange);
		return () => {
			window.removeEventListener("visibilitychange", handleVisibilityChange);
		};
	}, [badgeNum]);

	const toggleCollapsed = (): void => {
		dispatch({ type: "SET_SIDERCOLLAPSED", data: !siderCollapsed });
	};

	// 监听当前语言
	useEffect(() => {
		console.log(t(state.language), "当前语言===");
		setCurrentLang(t(state.language));
	}, [state.language]);

	const renderBread = (routes: RouteItemsType[]) => {
		return <Breadcrumb className="header-bread">
			{
				routes.map(item => {
					return <Breadcrumb.Item key={item.path}>{t(`route.${item.name}`)}</Breadcrumb.Item>;
				})
			}
		</Breadcrumb>;

	};
	return <div className="header-box">
		<div className="header-left-box">
			{
				!siderCollapsed ? <MenuFoldOutlined onClick={toggleCollapsed} className="header-left-icon" /> : <MenuUnfoldOutlined className="header-left-icon" onClick={toggleCollapsed} />
			}
			{
				list && list.length &&
				renderBread(list)
			}
		</div>

		<div className="header-right-box">
			<div className={"right-badge"} onClick={showMessage}>
				<Badge size="small" count={badgeNum} >
					<BellOutlined />
				</Badge>
			</div>
			<QuestionCircleOutlined onClick={handleTo4xDesign} />
			<div className="frame-box">
				<span onClick={openNotification}>Vue3.x</span>
				<span> / </span>
				<span className="active">React17.x</span>
			</div>
			<div >
				<Dropdown menu={{ items: DropdownItems, onClick: handleDropdown }} trigger={["click"]}>
					<Space>
						{currentLang}
						<DownOutlined />
					</Space>
				</Dropdown>
			</div>
		</div>
		<Drawer width={"30%"} placement="right" closable={false} onClose={drawerClose} open={visibile}>
			<Tabs
				defaultActiveKey="1"
				items={messageOption.map((item, i) => {
					return {
						label: (
							<span>
								<item.Icon />
								{item.label + "(" + item.value + ")"}
							</span>
						),
						key: item.key,
						children: item.key,
					};
				})}
			/>
		</Drawer>
	</div>;
};

export default HeaderCom;