/*
 * @Author: your name
 * @Date: 2021-11-25 16:12:24
 * @LastEditTime: 2023-05-09 16:12:41
 * @LastEditors: kang_mou kangh@gov-security.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\components\MySider\index.tsx
 */
import { Button, Menu } from "antd";
import { ReactElement, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";
import about from "../../../assets/svg/about.svg";
import UerInfoCom from "../../../components/UserInfo";
import "../../../locales/i18n";
import { RouteItemsType, siderRoutes } from "../../../router";
import { RootContext } from "../../../store";
import { _findRouteByLocation } from "../../../utils";
import "./index.less";
const { SubMenu } = Menu;

const MySider = (): ReactElement => {
	const { t } = useTranslation();
	const { state, dispatch } = useContext(RootContext);
	const { styleModel, siderCollapsed, theme } = state;
	const [defaultKey, setDefaultKey] = useState<string[]>([]);
	const location = useLocation();
	const navigator = useNavigate();


	const renderMenuItem = (routers: RouteItemsType[]) => {
		return routers.map(item => {
			if (item.children && item.children.length) {
				return <SubMenu key={item.path} title={t(`route.${item.name}`)}>
					{
						renderMenuItem(item.children)
					}
				</SubMenu>;
			} else {
				return <Menu.Item key={item.path}>{t(`route.${item.name}`)}</Menu.Item>;
			}
		});
	};
	const handleChangeByMenu = (e) => {
		navigator(e.key);
	};
	useEffect(() => {
		const routes = _findRouteByLocation(location.pathname, true);
		const defaultPath = routes.map(item => item.path);
		setDefaultKey(defaultPath);
	}, [location.pathname]);
	return (
		<div className={!siderCollapsed ? "sider-box" : "sider-box1"}>
			<Menu
				className="sider-menu"
				selectedKeys={defaultKey}
				mode={styleModel}
				theme={theme}
				onClick={handleChangeByMenu}
				inlineCollapsed={siderCollapsed}
			>
				<UerInfoCom siderWidthType={state.siderCollapsed} model={state.styleModel} />
				{
					renderMenuItem(siderRoutes)
				}
			</Menu>
			<div className="sider-btn-box">
				{
					siderCollapsed ?
						<img src={about} style={{ width: "16px", height: "16px" }} alt="" />
						: <Button type="link" className="btn-box-about">
							<img src={about} alt="" />
							<span>联系我们</span>
						</Button>
				}

				{/* <img style={{ width: "20px", height: "20px", fontSize: "20px" }} src={login_out} alt="" /> */}
				{/* <LoginOutlined style={{ width: "24px", height: "24px", color: "#fff", fontSize: "24px" }} /> */}
			</div>
		</div>
	);

};


export default MySider;