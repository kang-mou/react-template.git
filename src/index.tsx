/*
 * @Author: your name
 * @Date: 2021-11-23 22:49:20
 * @LastEditTime: 2023-04-25 23:33:35
 * @LastEditors: 康某 carl0608@126.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\index.ts
 */
import ReactDOM from "react-dom";
import "./assets/css/reset.css";
import App from "./views/App";

// 设置国际化

ReactDOM.render(
	<App />, document.getElementById("root"));
