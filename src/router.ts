/*
 * @Author: your name
 * @Date: 2021-11-25 23:23:51
 * @LastEditTime: 2023-05-08 17:31:59
 * @LastEditors: kang_mou kangh@gov-security.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\router.ts
 */

import React from "react";
import { Outlet } from "react-router-dom";
import Dashboard from "./views/dashboard";
import Formdrag from "./views/formdrag";
import Splinetool from "./views/splinetool";
import Watermark from "./views/watermark";
export interface RouteItemsType {
	path: string;
	title: string;
	name: string;  // 务必正确填写英文，作为国际化的key
	redirect?: string;
	index?: boolean;
	component?: React.FC;
	children?: RouteItemsType[];
	outlet?: any;
	hidden?: boolean;
	rules?: "admin" | "other"[];
}
export const siderRoutes: RouteItemsType[] = [
	{
		index: true,
		path: "/dashboard",
		title: "仪表盘",
		name: "Dashboard",
		component: Dashboard,
	},
	{
		path: "/functional",
		title: "功能列表",
		redirect: "/functional/formdrag",
		name: "Functional",
		children: [
			{
				path: "/functional/formdrag",
				title: "表格拖拽",
				name: "Form-Drag",
				component: Formdrag,
			},
			{
				path: "/functional/watermark",
				title: "高级表格",
				name: "Watermark",
				component: Watermark,
			},
			{
				path: "/splinetool/index",
				title: "可视化",
				name: "splinetool",
				component: Splinetool,
			},
		],
	},
	{
		path: "/404page",
		title: "404页面",
		name: "404page",
		component: React.lazy(() => import("./views/404Page"))
	}
];
export const WhiteRoutes: RouteItemsType[] = [
	{
		path: "/login",
		title: "登录",
		name: "login",
		hidden: true,
		component: React.lazy(() => import("./views/Login")), // lazy 会导致 看着想白屏 但是其实是加载了loading
	},
	{
		path: "/",
		title: "首页",
		name: "home",
		component: React.lazy(() => import("./layout/index")),
		outlet: Outlet,
		children: [...siderRoutes],
	},
	{
		path: "*",
		name: "404",
		title: "404",
		hidden: true,
		component: React.lazy(() => import("./views/404Page")),
	},
];
