/*
 * @Author: your name
 * @Date: 2021-11-25 23:25:03
 * @LastEditTime: 2021-11-25 23:26:08
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\interfaces\index.ts
 */
export interface KeyType<T> {
	[key: string]: T
}