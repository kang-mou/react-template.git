/*
 * @Author: carl
 * @Date: 2021-12-06 14:23:26
 * @LastEditTime: 2022-12-27 17:49:45
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @Description: 在使用仓库时会导致页面useState重新赋值 ，请谨慎使用
 * @FilePath: \react-template\src\store\index.ts
 */


import React from "react";
import { _restore } from "../utils";
import { storeType } from "./storeInterface";
export const RootStore: storeType = {
	test: 1,
	rootLoading: false,									//全局loading
	siderCollapsed: false,							//菜单栏宽度控制按钮
	breadcrumbList: [],									//面包屑tab数组
	theme: "dark",
	styleModel: "inline",
	language: "zh"
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const RootReducer = (state: typeof RootStore, action: { type: string, data: any }) => {
	switch (action.type) {
		case "SET_TEST":
			return {
				...state, test: action.data
			};
		case "SET_SIDERCOLLAPSED":
			return { ...state, siderCollapsed: action.data };
		case "SET_BREADCRUMBLIST": {
			const sessionArr = sessionStorage.getItem("ROUTE_CHUNKS") || "";
			/** 存一份再本地*/
			let arr = sessionArr && sessionArr !== "" ? JSON.parse(sessionArr) : [];
			arr = _restore(arr.concat([...action.data]), "path");
			sessionStorage.setItem("ROUTE_CHUNKS", JSON.stringify(arr));
			return { ...state, breadcrumbList: arr };
		}
		case "SET_STYLECOLOR":
			return { ...state, theme: action.data };
		case "SET_STYLEMODEL":
			return { ...state, styleModel: action.data };
		case "SET_LANGUAGE":
			return { ...state, language: action.data };
		default:
			return state;
	}
};

export const RootContext = React.createContext(null);