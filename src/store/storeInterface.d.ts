/*
 * @Author: your name
 * @Date: 2021-12-08 15:02:34
 * @LastEditTime: 2022-12-27 17:36:09
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @Description: 仓库接口定义
 * @FilePath: \react-template\src\store\storeInterface.d.ts
 */

/* 总数据 */
export interface storeType {
	[key: string]: any,
	rootLoading: boolean,
	siderCollapsed?: boolean,
	breadcrumbList: BreadcrumbItemType[],
	theme: string,
	styleModel: string,
	language: "zh" | "en"
}
/* 面包屑 */
export interface BreadcrumbItemType {
	path: string,
	search?: string,
	hash?: string,
	state?: string,
	key: string,
	title: string
}