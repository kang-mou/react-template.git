import {
	CloseOutlined
} from "@ant-design/icons";
import { Input } from "antd";
import React, { useState } from "react";
import "./index.less";
interface ToDolistType {
  className?: string;
}
const ToDolist = (props: ToDolistType): React.ReactElement => {
	const [value, setValue] = useState<string>("");
	const [list, setList] = useState<string[]>(["测试1", "测试2"]);

	const valueChange = (e) => {
		setValue(e.target.value);
	};
	const inputKeyUP = (e) => {
		let cpList = [...list];
		if (e.keyCode === 13) {
			cpList.push(value);
			setList(cpList);
			setValue("");
		}
	};
	const deletTodoList = (i: number) => {
		let copArr = JSON.parse(JSON.stringify(list));
		copArr.splice(i, 1);
		setList(copArr);
	};
	return (
		<div className={`todolist ${props.className || ""}`}>
			<div className="todo-title">
				<Input
					className="todo-input"
					value={value}
					placeholder={"Todo List"}
					onKeyUp={inputKeyUP}
					onChange={valueChange}
				></Input>
			</div>
			<div className="todo-content">
				{list.map((item, index) => {
					return (
						<div key={item + index} className="todo-content-item">
							<div className="todo-content-item-left"></div>
							<div className="todo-content-item-right">
								<span>{item}</span>
								<span className="todo-content-delet" >
									<CloseOutlined onClick={() => { deletTodoList(index); }} />
								</span>
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default ToDolist;
