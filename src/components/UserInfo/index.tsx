/*
 * @Author: your name
 * @Date: 2021-12-06 11:07:06
 * @LastEditTime: 2022-12-20 14:05:53
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\components\UserInfo\index.tsx
 */
import { Button } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import defaultImg from "../../assets/img/default.jpg";
import "./index.less";
interface UerInfoComType {
	siderWidthType?: boolean,
	model?: string,
}
const UerInfoCom = (props: UerInfoComType): React.ReactElement => {
	const navigate = useNavigate();
	const { t } = useTranslation();
	const logOut = () => {
		window.localStorage.clear();
		navigate("/login");
	};
	return (
		<>
			<div className="user_box user_color">
				<div className="user_log">
					<img src={defaultImg} className="user_img" />
					<Button className="out_btn" type="link" shape="circle" onClick={logOut}>
						{t("user.quit")}
					</Button>
				</div>
				{
					!props?.siderWidthType && <div className="user_signature" >
						<span >
							{t("user.brief")}
						</span>
					</div>
				}
			</div >
		</>
	);
};

export default UerInfoCom;
