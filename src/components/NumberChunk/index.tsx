/*
 * @Author: 康某 carl0608@126.com
 * @Date: 2022-08-12 21:58:10
 * @LastEditors: 康某 carl0608@126.com
 * @LastEditTime: 2023-04-25 23:57:45
 * @FilePath: \react-template\src\components\NumberChunk\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import CountUp from "react-countup";
import "./index.less";

interface NumberChunkType {
	num: number,
	icon?: React.ReactElement,
	title: string,
	width?: string,
	height: string
}

const NumberChunk = (props: NumberChunkType): React.ReactElement => {
	return <div className="number-chunk-box" style={{ width: "100%", height: props.height }}>
		<div className="chunk-icon">
			{
				props.icon
			}
		</div>
		<div className="chunk-title">
			<div className="chunk-subtitle">
				{props.title}
			</div>
			<div className="chunk-number">
				<CountUp
					start={0}
					end={props.num}
					useEasing={true}
					duration={1}
					decimals={0}
					separator=","
				/>
			</div>
		</div>
	</div>;
};

export default NumberChunk;