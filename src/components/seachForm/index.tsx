import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import React, { useEffect, useState } from "react";
import "./indexl.less";
export interface seachFormType {
	options: seachFormItem[]
}
export interface seachFormItem {
	type: string,
	props: string,
	label: string
}

const SeachFormCom = (props: seachFormType): React.ReactElement => {
	const [form] = Form.useForm();
	const [currentOptions, setCurrentOptions] = useState([]);
	const [btnState, setBtnState] = useState(false);
	const [state, setState] = useState(false);
	const optionsType = [
		{
			type: "input",
			props: "input",
			label: "input"
		},
		{
			type: "select",
			props: "select",
			data: [
				{
					label: "test",
					value: "test"
				}
			],
			label: "select"
		},
		{
			type: "date",
			props: "date",
			label: "date"
		},
		{
			type: "select",
			props: "selec1t",
			data: [
				{
					label: "test",
					value: "test"
				}
			],
			label: "select"
		},
		// {
		// 	type: "date",
		// 	props: "date1",
		// 	label: "date"
		// },
		// {
		// 	type: "select",
		// 	props: "select2",
		// 	data: [
		// 		{
		// 			label: "test",
		// 			value: "test"
		// 		}
		// 	],
		// 	label: "select"
		// },
		// {
		// 	type: "date",
		// 	props: "date2",
		// 	label: "date"
		// },
	];
	const renderBytype = (e) => {
		switch (e.type) {
			case "input":
				return <Input placeholder={"请输入"}></Input>;
			case "select":
				return <Select style={{ width: "200px" }} placeholder={"请选择"}>
					{
						e.data.map((item) => {
							return <Select.Option value={item.value} key={item.label + item.key}>{item.label}</Select.Option>;
						})
					}
				</Select>;
			case "date":
				return <DatePicker.RangePicker placeholder={["开始时间", "结束时间"]}></DatePicker.RangePicker>;
			default:
				return "";
		}

	};
	const renderBtn = () => {
		return <Col span={4}>
			<Form.Item className={btnState ? "" : "form-show-btn"}>
				<Button type={"primary"}>查 询</Button>
			</Form.Item>
		</Col>;
	};
	useEffect(() => {
		setCurrentOptions(optionsType);
		if (props.options && props.options.length) {
			//如果传过来的筛选条件数组大于4，则 按钮跟随表单
			if (props.options.length > 4) {
				setBtnState(true);
			} else {
				setBtnState(false);
			}
		}
	}, [props.options]);
	// //判断高度 超过 32px 隐藏
	// useEffect(() => {
	// 	const domc: any = document.querySelector(".form-box");
	// 	console.log(domc.offsetHeight, "获取筛选条件高度");
	// 	setFormHeight(domc.offsetHeight);
	// 	 if (domc.offsetHeight > 32) {
	// 	 	setBtnState(true);
	// 	 } else {
	// 	 	setBtnState(false);
	// 	 }
	// }, [formHeight]);
	return <div>
		<Form layout="inline" className="form-box">

			{
				<Row>
					{
						currentOptions && currentOptions.length && currentOptions.map((item, index) => {
							if (currentOptions && currentOptions.length > 4) {
								return <Col span={4} key={item.props}>
									{
										<Form.Item label={item.label} name={item.props} className={"form-item"}>
											{
												renderBytype(item)
											}
										</Form.Item>
									}
								</Col>;
							} else {
								return <>
									<Col span={4} key={item.props}>
										<Form.Item label={item.label} name={item.props} className={"form-item"}>
											{
												renderBytype(item)
											}
										</Form.Item>
									</Col>;
								</>;

							}
						})
					}
				</Row>
			}

		</Form>
	</div>;
};

export default SeachFormCom;