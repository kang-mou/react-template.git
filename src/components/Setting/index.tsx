/*
 * @Author: your name
 * @Date: 2021-11-25 16:12:24
 * @LastEditTime: 2022-12-27 09:48:45
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\components\Setting\index.tsx
 */
import { Drawer } from "antd";
import { ReactElement, useContext, useState } from "react";
import { RootContext } from "../../store";
import "./index.less";

interface nameType {
	list: {
		title: string;
		url: string;
		key: string
	}[];
	value: string
}

interface LayoutSettings {
	settings: string
}



const SettingsCheckBox = ({ list, value }: nameType): ReactElement => {
	const { dispatch } = useContext(RootContext);

	const styleClick = (e): void => {
		if (value === "style") {
			dispatch({ type: "SET_STYLECOLOR", data: e });
		} else {
			dispatch({ type: "SET_STYLEMODEL", data: e });
		}
	};

	return (
		<div>
			{
				list.map((item, index) => {
					return <div onClick={() => styleClick(item.key)} key={index}>
						<img src={item.url} />
						{item.title}
					</div>;
				})
			}
		</div >
	);
};

const Setting = (props: LayoutSettings): ReactElement => {
	const [visible, setVisible] = useState<boolean>(false);

	const onVisibleClick = () => {
		setVisible(!visible);
	};
	return (
		<>
			<Drawer
				title="Basic Drawer"
				placement="right"
				onClose={onVisibleClick}
				visible={visible}
				width={300}
				closable
			// handler={
			// 	<div
			// 		className="layout__settings"
			// 		style={{
			// 			color: "#fff",
			// 			fontSize: 20,
			// 		}}
			// 		onClick={onVisibleClick}
			// 	>
			// 		{visible ? <CloseOutlined /> : <SettingOutlined />}
			// 	</div>
			// }
			>
				<div>
					<h2>STYLE</h2>
					<SettingsCheckBox
						value="style"
						list={
							[
								{
									title: "暗黑风格",
									url: "https://gw.alipayobjects.com/zos/antfincdn/XwFOFbLkSM/LCkqqYNmvBEbokSDscrm.svg",
									key: "dark",
								},
								{
									title: "光亮风格",
									url: "https://gw.alipayobjects.com/zos/antfincdn/NQ%24zoisaD2/jpRkZQMyYRryryPNtyIC.svg",
									key: "light",
								}
							]
						}
					></SettingsCheckBox>
				</div>

				<div>
					<h2>MODEL</h2>
					<SettingsCheckBox
						value="model"
						list={
							[
								{
									title: "侧边菜单布局",
									url: "https://gw.alipayobjects.com/zos/antfincdn/XwFOFbLkSM/LCkqqYNmvBEbokSDscrm.svg",
									key: "inline",
								},
								{
									title: "顶部菜单布局",
									url: "https://gw.alipayobjects.com/zos/antfincdn/URETY8%24STp/KDNDBbriJhLwuqMoxcAr.svg",
									key: "horizontal",
								}
							]
						}
					></SettingsCheckBox>
				</div>
			</Drawer>
		</ >
	);
};

export default Setting;
