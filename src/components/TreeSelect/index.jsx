import React, { useState, useEffect } from "react";
import { Tree, Input, Dropdown, Menu, Modal, Form, Radio, Button, notification } from "antd";


const { Search } = Input;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const TreeSelect = prop => {
	console.log(prop);
	const { renderData } = prop;

	const [unfolds, setUnfolds] = useState([]);
	const [autoExpandParent, setAutoExpandParent] = useState(false);
	const [antistop, setAntistop] = useState(false);

	//tree结构的递归（二维数组的情况）
	const mapTreeData = (data) => {
		return data.map((item) => {
			return {
				title: item.name,
				key: item.id,
				level: item.level,
				children: item.level >= 2 || item.children == null ? [] : mapTreeData(item.children)
			};
		});
	};

	const loop = data =>
		data.map(item => {
			const index = item.title.indexOf(antistop);
			const title =
        index > -1 ? (
        	<span>
        		<span style={item.title.includes(antistop) ? { color: "rgb(255,85,0)" } : {}}>{item.title}</span>

        	</span>
        ) : (
        	<span>{item.title}</span>
        );
			if (item.children) {
				return { title, key: item.key, level: item.level, children: loop(item.children) };
			}

			return {
				title,
				key: item.key,
				level: item.level,
			};
		});

	const onExpand = expandedKeys => {
		setAutoExpandParent(false);
		setUnfolds(expandedKeys);
	};

	/* eslint-disable */
  const getKey = (data, cun, value, expandedKeys) => {
    data.map((item) => {
      cun = cun + ',' + item.key;
      if (item.title.indexOf(value) > -1) {
        expandedKeys.push(...cun.split(','))
      }
      if (item.hasOwnProperty('children')) {
        getKey(item.children, cun, value, expandedKeys);
      }
    })
  }

  //输入框关键词搜索
  const onChange = (e) => {
    const { value } = e.target;
    console.log(value)
    const expandedKeys = [];
    if (value === "") {
      setAntistop(false);
      setUnfolds(false);
      return null;
    }
    mapTreeData(renderData).map(item => {
      let cun = item.key;
      getKey(item.children, cun, value, expandedKeys);
    });

    setAntistop(value);
    setAutoExpandParent(true);
    setUnfolds([...new Set(expandedKeys)]);
  }
  return <div>
    <div>
     <Search placeholder="请搜索" onChange={onChange} />
    </div>
  
    <Tree
      icon={<></>}
      showIcon={true}
      onExpand={onExpand}
      expandedKeys={unfolds}
      autoExpandParent={autoExpandParent}
      treeData={loop(mapTreeData(renderData))}
    />
  </div>;
};

export default(TreeSelect);