/*
 * @Author: 康某 carl0608@126.com
 * @Date: 2022-08-12 21:58:10
 * @LastEditors: 康某 carl0608@126.com
 * @LastEditTime: 2023-04-26 00:13:26
 * @FilePath: \react-template\src\components\EchartsCom\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as echarts from "echarts";
import React, { useEffect } from "react";

export interface EchartsComType {
	id: string,
	option: any,
	className?: string,
	width: string,
	height: string
}
const EchartsCom = (props: EchartsComType): React.ReactElement => {

	useEffect(() => {
		const dom = echarts.init(document.getElementById(props.id));
		dom.setOption(props.option);
	}, []);
	return <div id={props.id} className={props.className} style={{ width: props.width, height: props.height }}></div>;

};

export default EchartsCom;