/*
 * @Author: 康浩 hao.kang02@geely.com
 * @Date: 2022-01-07 14:13:16
 * @LastEditors: 康某 carl0608@126.com
 * @LastEditTime: 2023-05-05 22:09:02
 * @FilePath: \react-template\src\views\dashboard\componets\lineEach.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import * as echarts from "echarts";
import React, { useContext, useEffect, useState } from "react";
import { RootContext } from "../../../store";
const LineEach = (): React.ReactElement => {
	const { state } = useContext(RootContext);
	const expectedData = [100, 120, 161, 134, 105, 160, 165];
	const actualData = [120, 82, 91, 154, 162, 140, 145];
	const [dom, setDom] = useState(null);
	const renderLine = () => {
		const dom = echarts.init(document.getElementById("LineEach"));
		setDom(dom);
		dom.setOption({
			xAxis: {
				data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
				boundaryGap: false,
				axisTick: {
					show: false
				}
			},
			grid: {
				left: 10,
				right: 10,
				bottom: 20,
				top: 30,
				containLabel: true
			},
			tooltip: {
				trigger: "axis",
				axisPointer: {
					type: "cross"
				},
				padding: [5, 10]
			},
			yAxis: {
				axisTick: {
					show: false
				}
			},
			legend: {
				data: ["expected", "actual"]
			},
			series: [{
				name: "expected", itemStyle: {
					normal: {
						color: "#FF005A",
						lineStyle: {
							color: "#FF005A",
							width: 2
						}
					}
				},
				smooth: true,
				type: "line",
				data: expectedData,
				animationDuration: 2800,
				animationEasing: "cubicInOut"
			},
			{
				name: "actual",
				smooth: true,
				type: "line",
				itemStyle: {
					normal: {
						color: "#3888fa",
						lineStyle: {
							color: "#3888fa",
							width: 2
						},
						areaStyle: {
							color: "#f3f8ff"
						}
					}
				},
				data: actualData,
				animationDuration: 2800,
				animationEasing: "quadraticOut"
			}]
		});
	};
	const lineRise = () => {
		// console.log(dom);
		if (!dom) return;
		// dom.clear();
		dom.resize();
	};
	useEffect(() => {
		lineRise();
	}, [state.siderCollapsed]);
	useEffect(() => {
		renderLine();
		window.addEventListener("resize", () => {
			lineRise();
		});
	}, [dom]);

	return <div id="LineEach" style={{ width: "100%", height: "350px" }} />;

};
export default LineEach;