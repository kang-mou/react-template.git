/*
 * @Author: kang_mou kangh@gov-security.com
 * @Date: 2023-02-23 16:55:15
 * @LastEditors: kang_mou kangh@gov-security.com
 * @LastEditTime: 2023-05-09 13:25:04
 * @FilePath: \react-template\src\views\dashboard\componets\imgCom.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import city from "../../../assets/img/city.jpg";

const ImgCom = (): React.ReactElement => {
	return <div className="img-com">
		<img src={city} alt="" />
	</div>;
};
export default ImgCom;