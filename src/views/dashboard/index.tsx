/*
 * @Author: your name
 * @Date: 2021-12-02 16:23:51
 * @LastEditTime: 2021-12-02 16:25:11
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\dashboard\index.tsx
 */
import {
	BulbOutlined,
	CarOutlined,
	DollarOutlined,
	GithubOutlined
} from "@ant-design/icons";
import React from "react";
import EchartsCom from "../../components/EchartsCom";
import NumberChunk from "../../components/NumberChunk";
import ToDolist from "../../components/ToDolist";
import ImgCom from "./componets/imgCom";
import { BarChart, PieChart, RaddarChat } from "./componets/index";
import LineEach from "./componets/lineEach";
import DasTable from "./componets/table";
import "./index.less";
const Dashboard = (): React.ReactElement => {
	return (
		<div className="dashboard-box">
			<div className="number-box">
				<NumberChunk
					icon={<GithubOutlined />}
					width={100 / 4 - 2 + "%"}
					height="108px"
					title="New-Message"
					num={10244}
				></NumberChunk>
				<NumberChunk
					icon={<BulbOutlined />}
					width={100 / 4 - 2 + "%"}
					height="108px"
					title="New-Message"
					num={10244}
				></NumberChunk>
				<NumberChunk
					icon={<CarOutlined />}
					width={100 / 4 - 2 + "%"}
					height="108px"
					title="New-Message"
					num={10244}
				></NumberChunk>
				<NumberChunk
					icon={<DollarOutlined />}
					width={100 / 4 - 2 + "%"}
					height="108px"
					title="New-Message"
					num={10244}
				></NumberChunk>
			</div>
			<div className="dashboard-line-ec">
				<LineEach></LineEach>
			</div>
			<div className="dashboard-ecs">
				<EchartsCom
					className="RaddarChat"
					width="100%"
					height="300px"
					id="echar1"
					option={RaddarChat}
				></EchartsCom>
				<EchartsCom
					className="RaddarChat"
					width="100%"
					height="300px"
					id="echar2"
					option={PieChart}
				></EchartsCom>
				<EchartsCom
					className="RaddarChat"
					width="100%"
					height="300px"
					id="echar3"
					option={BarChart}
				></EchartsCom>
			</div>
			<div className="dashboard-table">
				<DasTable></DasTable>
				<ToDolist className="todo_box"></ToDolist>
				<ImgCom></ImgCom>
			</div>
		</div>
	);
};
export default Dashboard;
