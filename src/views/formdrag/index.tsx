/*
 * @Author: your name
 * @Date: 2021-11-25 23:38:47
 * @LastEditTime: 2023-05-09 11:09:28
 * @LastEditors: kang_mou kangh@gov-security.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\test\index.tsx
 */
import type { ActionType, ProColumns, } from "@ant-design/pro-components";
import { DragSortTable, TableDropdown } from "@ant-design/pro-components";
import { Space, Tag } from "antd";

import React, { useEffect, useRef, useState } from "react";
import "./index.less";
type GithubIssueItem = {
	url: string;
	id: number;
	number: number;
	title: string;
	labels: {
		name: string;
		color: string;
	}[];
	state: string;
	comments: number;
	created_at: string;
	updated_at: string;
	closed_at?: string;
};
const FormDragPage = (): React.ReactElement => {
	const talRef = useRef(null);
	const [tabDom, setTabDom] = useState(null);

	const dataSource: any[] = [
		{
			key: "1",
			name: "胡彦斌",
			age: 32,
			address: "西湖区湖底公园1号",
		},
		{
			key: "2",
			name: "胡彦祖1",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "3",
			name: "胡彦祖2",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "4",
			name: "胡彦祖3",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "5",
			name: "胡彦祖4",
			age: 42,
			address: "西湖区湖底公园1号",
		},
	];

	const actionRef = useRef<ActionType>();

	const columns: ProColumns<GithubIssueItem>[] = [
		{
			dataIndex: "index",
			title: "序号",
			valueType: "indexBorder",
			width: 48,
		},
		{
			title: "标题",
			dataIndex: "title",
			copyable: true,
			ellipsis: true,
			tip: "标题过长会自动收缩",
			formItemProps: {
				rules: [
					{
						required: true,
						message: "此项为必填项",
					},
				],
			},
		},
		{
			disable: true,
			title: "状态",
			dataIndex: "state",
			filters: true,
			onFilter: true,
			ellipsis: true,
			valueType: "select",
			valueEnum: {
				all: { text: "超长".repeat(50) },
				open: {
					text: "未解决",
					status: "Error",
				},
				closed: {
					text: "已解决",
					status: "Success",
					disabled: true,
				},
				processing: {
					text: "解决中",
					status: "Processing",
				},
			},
		},
		{
			disable: true,
			title: "标签",
			dataIndex: "labels",
			search: false,
			renderFormItem: (_, { defaultRender }) => {
				return defaultRender(_);
			},
			render: (_, record) => (
				<Space>
					{record?.labels?.map(({ name, color }) => (
						<Tag color={color} key={name}>
							{name}
						</Tag>
					))}
				</Space>
			),
		},
		{
			title: "创建时间",
			key: "showTime",
			dataIndex: "created_at",
			valueType: "date",
			sorter: true,
			hideInSearch: true,
		},
		{
			title: "创建时间",
			dataIndex: "created_at",
			valueType: "dateRange",
			hideInTable: true,
			search: {
				transform: (value) => {
					return {
						startTime: value[0],
						endTime: value[1],
					};
				},
			},
		},
		{
			title: "操作",
			valueType: "option",
			key: "option",
			render: (text, record, _, action) => [
				<a
					key="editable"
					onClick={() => {
						action?.startEditable?.(record.id);
					}}
				>
					编辑
				</a>,
				<a href={record.url} target="_blank" rel="noopener noreferrer" key="view">
					查看
				</a>,
				<TableDropdown
					key="actionGroup"
					onSelect={() => action?.reload()}
					menus={[
						{ key: "copy", name: "复制" },
						{ key: "delete", name: "删除" },
					]}
				/>,
			],
		},
	];

	const options = [];

	useEffect(() => {
		const doc = document.querySelector(".ant-table-tbody");
		setTabDom(doc);
	}, []);
	return <div className="formdrag-box">
		<DragSortTable rowKey={"key"} dragSortKey={"title"} dataSource={dataSource} columns={columns} />
	</div>;


};

export default FormDragPage;