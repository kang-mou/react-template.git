/*
 * @Author: your name
 * @Date: 2021-11-23 22:52:42
 * @LastEditTime: 2023-05-05 22:47:14
 * @LastEditors: 康某 carl0608@126.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\App.tsx
 */
import { Alert, ConfigProvider, Spin } from "antd";
// import enUS from "antd/lib/locale/en_US";
// import zhCN from "antd/lib/locale/zh_CN";
import React, { Suspense, useReducer } from "react";
import { HashRouter, Route, Routes } from "react-router-dom";
import "../locales/i18n"; // 注入中英文切换
import { RouteItemsType, WhiteRoutes } from "../router";
import { RootContext, RootReducer, RootStore } from "../store/index";
import "./index.less";
const App = (): React.ReactElement => {
	const [state, dispatch] = useReducer(RootReducer, RootStore);
	const renderRoutItem = (e: RouteItemsType[]): any => {
		return e && e.length > 0 && e.map((item: RouteItemsType) => {
			if (!item.children) {
				return <Route key={item.name} path={item.path} index={item.index} element={<item.component />}></Route>;
			} else {
				if (item.children && item.children.length) {
					return renderRoutItem(item.children);
				}
				//  else {
				// 	return <div key={item.name}></div>;
				// }
			}
		});
	};
	return <div className={"app"}>
		<RootContext.Provider value={{ state, dispatch }} >
			{/* <ConfigProvider locale={state.language === "zh" ? zhCN : enUS}> */}
			<ConfigProvider>
				<HashRouter>
					<Suspense fallback={
						<div className="login-box">
							<Spin
								tip="Loading..." spinning={true}>
								<Alert
									message="Alert message title"
									description="Further details about the context of this alert."
									type="info"
								/>
							</Spin>
						</div>}>
						<Routes>
							{WhiteRoutes.map(item => {
								if (item.outlet) {
									return <Route key={item.name} path={item.path} index={item.index} element={<item.component />}>
										{
											item.children && item.children.length && renderRoutItem(item.children)
										}
									</Route>;
								} else {
									return <Route key={item.name} path={item.path} index={item.index} element={<item.component />}></Route>;
								}
							})
							}
						</Routes>
					</Suspense >
				</HashRouter>
			</ConfigProvider>
		</RootContext.Provider>
	</div >;
};

export default App;