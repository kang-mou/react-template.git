/*
 * @Author: your name
 * @Date: 2021-11-24 14:54:01
 * @LastEditTime: 2022-12-18 21:01:41
 * @LastEditors: 康某 carl0608@126.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\Login\index.tsx
 */
import { Button, Form, Input } from "antd";
import { ReactElement } from "react";
import { useNavigate } from "react-router-dom";
import "./index.less";
const LoginPage = (): ReactElement => {
	const navigate = useNavigate();
	const onFinish = (values: any) => {
		console.log("Success:", values);
		if (values.password === "admin" && values.username === "admin") {
			window.localStorage.setItem("token", "qunimade");
			navigate("/dashboard");
		}
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log("Failed:", errorInfo);
	};
	return <div className="login_box">
		<div className="login_content" >
			{/* <h1 className="login_title">不忘初心 牢记使命</h1> */}
			<Form
				name="basic"
				labelCol={{ span: 8 }}
				wrapperCol={{ span: 16 }}
				initialValues={{ remember: true }}
				onFinish={onFinish}
				onFinishFailed={onFinishFailed}
				autoComplete="off"
				className="login-form"
			>
				<Form.Item
					label=""
					className="form-item"
					name="username"
					rules={[{ required: true, message: "Please input your username!" }]}
				>
					<Input
						className="login-form-item"
						placeholder="账号"
					/>
				</Form.Item>

				<Form.Item
					label=""
					name="password"
					className="form-item"
					rules={[{ required: true, message: "Please input your password!" }]}
				>
					<Input.Password
						placeholder="密码"
						className="login-form-item" />
				</Form.Item>

				{/* <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
					<Checkbox>Remember me</Checkbox>
				</Form.Item> */}

				<Form.Item className="form-item">
					<Button type="text" htmlType="submit" className="login_btn">
						登录
					</Button>
				</Form.Item>
			</Form>
		</div>
	</div >;
};
export default LoginPage;