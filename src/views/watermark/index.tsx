/*
 * @Author: 康浩 hao.kang02@geely.com
 * @Date: 2022-07-14 15:32:21
 * @LastEditors: kang_mou kangh@gov-security.com
 * @LastEditTime: 2023-05-09 11:31:52
 * @FilePath: \react-template\src\views\selectTree\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { EllipsisOutlined, PlusOutlined } from "@ant-design/icons";
import type { ActionType, ProColumns, } from "@ant-design/pro-components";
import { ProTable, TableDropdown } from "@ant-design/pro-components";
import { Button, DatePicker, Dropdown, Space, Tag } from "antd";
import React, { useRef } from "react";
// import WaterMark from "watermark-component-for-react";
import "./index.less";

const { RangePicker } = DatePicker;
type GithubIssueItem = {
	url: string;
	id: number;
	number: number;
	title: string;
	labels: {
		name: string;
		color: string;
	}[];
	state: string;
	comments: number;
	created_at: string;
	updated_at: string;
	closed_at?: string;
};
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const waitTimePromise = async (time: number = 100) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(true);
		}, time);
	});
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const waitTime = async (time: number = 100) => {
	await waitTimePromise(time);
};
const index = (): React.ReactElement => {
	const actionRef = useRef<ActionType>();
	const columns: ProColumns<GithubIssueItem>[] = [
		{
			dataIndex: "index",
			valueType: "indexBorder",
			width: 48,
		},
		{
			title: "标题",
			dataIndex: "title",
			copyable: true,
			ellipsis: true,
			tip: "标题过长会自动收缩",
			formItemProps: {
				rules: [
					{
						required: true,
						message: "此项为必填项",
					},
				],
			},
		},
		{
			disable: true,
			title: "状态",
			dataIndex: "state",
			filters: true,
			onFilter: true,
			ellipsis: true,
			valueType: "select",
			valueEnum: {
				all: { text: "超长".repeat(50) },
				open: {
					text: "未解决",
					status: "Error",
				},
				closed: {
					text: "已解决",
					status: "Success",
					disabled: true,
				},
				processing: {
					text: "解决中",
					status: "Processing",
				},
			},
		},
		{
			disable: true,
			title: "标签",
			dataIndex: "labels",
			search: false,
			renderFormItem: (_, { defaultRender }) => {
				return defaultRender(_);
			},
			render: (_, record) => (
				<Space>
					{record?.labels?.map(({ name, color }) => (
						<Tag color={color} key={name}>
							{name}
						</Tag>
					))}
				</Space>
			),
		},
		{
			title: "创建时间",
			key: "showTime",
			dataIndex: "created_at",
			valueType: "date",
			sorter: true,
			hideInSearch: true,
		},
		{
			title: "创建时间",
			dataIndex: "created_at",
			valueType: "dateRange",
			hideInTable: true,
			search: {
				transform: (value) => {
					return {
						startTime: value[0],
						endTime: value[1],
					};
				},
			},
		},
		{
			title: "操作",
			valueType: "option",
			key: "option",
			render: (text, record, _, action) => [
				<a
					key="editable"
					onClick={() => {
						action?.startEditable?.(record.id);
					}}
				>
					编辑
				</a>,
				<a href={record.url} target="_blank" rel="noopener noreferrer" key="view">
					查看
				</a>,
				<TableDropdown
					key="actionGroup"
					onSelect={() => action?.reload()}
					menus={[
						{ key: "copy", name: "复制" },
						{ key: "delete", name: "删除" },
					]}
				/>,
			],
		},
	];
	const dataSource: any = [
		{
			key: "1",
			name: "胡彦斌",
			age: 32,
			address: "西湖区湖底公园1号",
		},
		{
			key: "2",
			name: "胡彦祖1",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "3",
			name: "胡彦祖2",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "4",
			name: "胡彦祖3",
			age: 42,
			address: "西湖区湖底公园1号",
		},
		{
			key: "5",
			name: "胡彦祖4",
			age: 42,
			address: "西湖区湖底公园1号",
		},
	];
	return (
		<>
			<ProTable<GithubIssueItem>
				columns={columns}
				actionRef={actionRef}
				dataSource={dataSource}
				cardBordered
				request={() => {
					return new Promise((resolve, reject) => {
						resolve({
							total: 0,
							data: [],
							success: true
						});
					});
				}}
				editable={{
					type: "multiple",
				}}
				columnsState={{
					persistenceKey: "pro-table-singe-demos",
					persistenceType: "localStorage",
					onChange(value) {
						console.log("value: ", value);
					},
				}}
				rowKey="id"
				search={{
					labelWidth: "auto",
				}}
				options={{
					setting: {
						listsHeight: 400,
					},
				}}
				form={{
					// 由于配置了 transform，提交的参与与定义的不同这里需要转化一下
					syncToUrl: (values, type) => {
						if (type === "get") {
							return {
								...values,
								created_at: [values.startTime, values.endTime],
							};
						}
						return values;
					},
				}}
				pagination={{
					pageSize: 5,
					onChange: (page) => console.log(page),
				}}
				dateFormatter="string"
				headerTitle="高级表格"
				toolBarRender={() => [
					<Button
						key="button"
						icon={<PlusOutlined />}
						onClick={() => {
							actionRef.current?.reload();
						}}
						type="primary"
					>
						新建
					</Button>,
					<Dropdown
						key="menu"
						menu={{
							items: [
								{
									label: "1st item",
									key: "1",
								},
								{
									label: "2nd item",
									key: "1",
								},
								{
									label: "3rd item",
									key: "1",
								},
							],
						}}
					>
						<Button>
							<EllipsisOutlined />
						</Button>
					</Dropdown>,
				]}
			/>
		</>
	);
};

export default index;
