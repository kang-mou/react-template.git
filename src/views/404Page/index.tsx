/*
 * @Author: your name
 * @Date: 2021-12-02 17:49:57
 * @LastEditTime: 2022-12-18 20:35:12
 * @LastEditors: 康某 carl0608@126.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\src\views\404Page\index.tsx
 */
import React from "react";
import imgSrc from "../../assets/img/404.png";
import "./index.less";
const Page404 = (): React.ReactElement => {
	return <div className="page404-box">
		<img src={imgSrc} alt="" />
	</div>;
};
export default Page404;