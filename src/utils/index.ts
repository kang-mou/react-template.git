/*
 * @Author: your name
 * @Date: 2021-12-08 17:16:18
 * @LastEditTime: 2021-12-13 10:05:57
 * @LastEditors: Please set LastEditors
 * @Description: 工具函数
 * @FilePath: \react-template\src\utils\index.ts
 */
import { RouteItemsType, siderRoutes } from "../router";
/* 数组去重 */
export const _restore = (data: any[], name?: string): any[] => {

	if (data.length) {
		let obj: any = {};
		let peon = data.reduce((cur: any, next: any) => {
			// eslint-disable-next-line no-unused-expressions
			obj[next[name]] ? "" : obj[next[name]] = true && cur.push(next);
			return cur;
		}, []);
		return peon;
	} else {
		return [];
	}
};

/* 根据路由匹配到当前路由的配置 */

export const _findRouteByLocation = (path: string, isPare?: boolean): RouteItemsType[] => {
	let routes: RouteItemsType[] = [];
	siderRoutes.map((item) => {
		if (item.path === path) {
			routes.push(item);
		} else if (item.children) {
			item.children.map((item2) => {
				if (item2.path === path) {
					isPare && routes.push(item);
					routes.push(item2);
				}
			});
		}
	});
	return routes;
};