/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/*
 * @Author: kang_mou kangh@gov-security.com
 * @Date: 2023-03-29 13:27:23
 * @LastEditors: kang_mou kangh@gov-security.com
 * @LastEditTime: 2023-03-30 13:19:49
 * @FilePath: \react-template\src\utils\resize.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// 需要在body 添加一个样式 transform-origin: left top;  // 出现白边 请自行调整 pageWidth、pageHeight
export default () => {
	console.log("是否触发===");
	const screenWidth = window.innerWidth;
	const screenHeight = window.innerHeight;
	let pageWidth = 1463;
	let pageHeight = 772;
	// let rootDiv = document.getElementById('root')
	document.body.style.width = `${pageWidth}px`;
	document.body.style.height = `${pageHeight}px`;
	const ratioX = screenWidth / pageWidth;
	const ratioY = screenHeight / pageHeight;
	const scale = Math.min(ratioX, ratioY);
	let offsetX = 0;
	let offsetY = 0;
	if (ratioX < ratioY) {
		offsetY = (screenHeight / ratioX - pageHeight) / 2;
	} else {
		offsetX = (screenWidth / ratioY - pageWidth) / 2;
	}
	document.body.style.transform = `scale(${scale})`;
	(window as any)?.globalScale && (window as any)?.globalScale.setScale(scale, scale);
};