/*
 * @Author: 康浩 hao.kang02@geely.com
 * @Date: 2022-12-27 15:37:20
 * @LastEditors: 康浩 hao.kang02@geely.com
 * @LastEditTime: 2022-12-27 17:46:42
 * @FilePath: \react-template\src\locales\i18n.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import enUsTrans from "./en.json";
import zhCnTrans from "./zh.json";
// import LanguageDetector from 'i18next-browser-languagedetector'; // 自动根据当前环境设置语言

i18n.use(initReactI18next).init({
	resources: {
		en: {
			translation: enUsTrans,
		},
		zh: {
			translation: zhCnTrans,
		},
	},
	//默认语言
	lng: "zh",
	debug: false,
	interpolation: {
		escapeValue: false,
	},
});
// i18n.use(LanguageDetector)

export default i18n;