/*
 * @Author: your name
 * @Date: 2021-11-23 22:34:47
 * @LastEditTime: 2023-05-08 13:17:10
 * @LastEditors: kang_mou kangh@gov-security.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \react-template\config\index.ts
 */

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// const FriendlyPlugin = require("friendly-errors-webpack-plugin");
const CompressionWebpackPlugin = require("compression-webpack-plugin"); // gzip
const { CleanWebpackPlugin } = require("clean-webpack-plugin"); // 打包前清除 dist
const webpack = require("webpack");
console.log(process.env.NODE_ENV, "当前环境变量====");
module.exports = {
  entry: path.resolve(__dirname, "../src/index.tsx"),
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "[name].js",
    // publicPath: "static/",
    clean: true,
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".less"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      // 配置 file-loader
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: {
          loader: "file-loader",
          options: {
            esModule: false,
          },
        },
        type: "javascript/auto",
      },
      {
        test: /\.(jsx|js|ts|tsx)$/,
        include: [path.resolve(__dirname, "../src")],
        exclude: [/node_modules/],
        use: ["eslint-loader"],
        enforce: "pre",
      },
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-env"],
        },
        exclude: /node_modules/,
      },

      {
        test: /\.(less|css)$/,
        use: ["style-loader", "css-loader", "less-loader", "postcss-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  stats: "errors-only",

  devServer: {
    port: 8088, // 端口号
    hot: true,
    // open: true,
    compress: true, // 开启gzip 压缩
    // publickPath: "./",
    client: {
      logging: "error", // 设置错误日志级别
      // overlay: true, // 当编译出现问题的时候 错误警告会全屏覆盖
      progress: true, // 进度条
      overlay: {
        errors: true,
        warnings: false,
      },
    },
    proxy: {
      "/api": {
        target: "http://localhost:3000",
        pathRewrite: { "^/api": "" },
      },
    },
  },
  // target: ["web", "es5"],
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development"),
      },
    }),
    // 打包前清除dist 文件
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: true,
      protectWebpackAssets: false,
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "../public/index.html"),
    }),

    //   new FriendlyPlugin({
    //    // 优化 控制台输出太多东西
    //    compilationSuccessInfo: {
    //     messages: [`Your application is running here: http://localhost:8088`],
    //    },
    //    // 是否每次都清空控制台
    //    clearConsole: true,
    //   }),
    // 开启gzip
    new CompressionWebpackPlugin({
      exclude: /.(html|map)$/i, //排除 html 、map
    }),
  ],
};
